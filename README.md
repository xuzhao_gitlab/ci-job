# ci-job

## 项目介绍：跑frontend endpoint to endpoint autotest cases

#### 项目结构
- common           # 公共类目录
- -- common.py     # 公共类文件
- pages            # 页面
- -- demo          # 封装的demo页面
- -- xrayDetail    # 封装的xrayDetail页面
- -- -- element.py
- -- -- locators.py
- -- -- page.py
- testcases        # 测试用例
- -- xrayDetailCase.py
- gitlab-ci.yml    # pipeline脚本
- requirements.txt # 依赖包文件
- run.py           # 批量执行脚本
- settings         # 配置文件


#### 执行步骤(linux环境)
1.安装Python环境：sudo apt install python3.8  
查看安装结果：python3.8 --version  
查看安装路径：which python3

2.安装Chrome浏览器和Chromedriver  
a.默认已经安装  
b.查看安装版本：google-chrome --version（90.0.4430.24）  
c.安装驱动：wget https://chromedriver.storage.googleapis.com/90.0.4430.24/chromedriver_linux64.zip  
d.放到/usr/bin/下面，给予可执行权限  
sudo mv chromedriver /usr/bin/chromedriver  
sudo chown root:root /usr/bin/chromedriver  
sudo chmod +x /usr/bin/chromedriver

3.安装Python依赖：pip install -r requirements.txt

4.执行测试用例：python3 testcases/demo.py
