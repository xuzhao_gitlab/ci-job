 # coding=utf-8
# @Author   : xuzhaozhou
# @Time     : 2021/10/9 下午5:00
# @Function :


from selenium.webdriver.common.by import By

class MainPageLocators(object):
    """A class for main page locators. All main page locators should come here"""
    PlanningButton = (By.LINK_TEXT, 'Planning')
    # ControlButton = (By.LINK_TEXT, 'Control')
    ControlButton = (By.ID, 'react-tabs-2')
    LatencyButton = (By.LINK_TEXT, 'Latency')
    DetailPanelButton = (By.LINK_TEXT, 'DetailPanel')
    KeyFramesButton = (By.LINK_TEXT, 'KeyFrames')
    ObstacleHistoryButton = (By.LINK_TEXT, 'ObstacleHistory')

    OkButton = (By.CLASS_NAME, 'MuiButton-label')




class SearchResultsPageLocators(object):
    """A class for search results locators. All search results locators should come here"""
    pass