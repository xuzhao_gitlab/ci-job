# coding=utf-8
# @Author   : xuzhaozhou
# @Time     : 2021/10/9 下午5:03
# @Function :

from .element import BasePageElement
from .locators import MainPageLocators

class SearchTextElement(BasePageElement):
    """This class gets the search text from the specified locator"""

    #The locator for search box where search string is entered
    locator = 'q'


class BasePage(object):
    """Base class to initialize the base page that will be called from all pages"""

    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):
    """Home page action methods come here. I.e. Python.org"""

    #Declares a variable that will contain the retrieved text
    search_text_element = SearchTextElement()

    def is_title_matches(self):
        """Verifies that the hardcoded text "xRay" appears in page title"""
        return "xRay" in self.driver.title

    # def clickPlanningButton(self):
    #     """Triggers the search"""
    #     element = self.driver.find_element(*MainPageLocators.PlanningButton)
    #     element.click()

    def checkResult(self):
        # print(self.driver.page_source)
        return "Scenario History" in self.driver.page_source

    def clickControlButton(self):
        element = self.driver.find_element(*MainPageLocators.ControlButton)
        element.click()

    def checkControlButtonResult(self):
        # print(self.driver.page_source)
        return "Trajectory" in self.driver.page_source

    def clickOkButton(self):
        element = self.driver.find_element(*MainPageLocators.OkButton)
        element.click()


class SearchResultsPage(BasePage):
    """Search results page action methods come here"""

    def is_results_found(self):
        # Probably should search for this text in the specific page
        # element, but as for now it works fine
        return "No results found." not in self.driver.page_source
