# coding=utf-8
# @Author   : xuzhaozhou
# @Time     : 2021/10/9 下午5:00
# @Function :


from selenium.webdriver.common.by import By

class MainPageLocators(object):
    """A class for main page locators. All main page locators should come here"""
    GO_BUTTON = (By.ID, 'submit')

class SearchResultsPageLocators(object):
    """A class for search results locators. All search results locators should come here"""
    pass