# coding=utf-8
# @Author   : xuzhaozhou
# @Time     : 2021/10/9 下午5:38
# @Function : XrayDetailPage测试用例文件

import unittest
import sys
import os
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)
from pages.xrayDetail.page import *
from common import common
from settings import local_url, test_url        # 导入配置文件中的本地域名和测试环境域名


class XrayDetailPage(unittest.TestCase):
    """A sample test class to show how page object works"""

    def setUp(self):
        # 引入common类的is_driver方法判断运行环境
        self.driver = common.CommonBase().is_driver()
        url = test_url + "?id=playback/accord-cn-3/20211014/2021-10-14-15-04-38"
        self.driver.get(url)
        self.driver.maximize_window()           # 最大化窗口

    def testSelectPlanning(self):
        # Load the main page. In this case the home page of Python.org.
        main_page = MainPage(self.driver)
        # Checks if the word "xRay" is in title
        assert main_page.is_title_matches(), "title doesn't match."

        # Click the Planning button
        self.driver.implicitly_wait(5)
        main_page.clickOkButton()
        # main_page.clickPlanningButton()
        # assert main_page.checkResult(), "body haven't Scenario History"

    def testSelectControl(self):
        # Load the main page. In this case the home page of Python.org.
        main_page = MainPage(self.driver)
        self.driver.implicitly_wait(5)
        main_page.clickOkButton()
        main_page.clickControlButton()
        # assert main_page.checkControlButtonResult(), "body haven't Trajectory"

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()