# coding=utf-8
'''
批量执行TestCases文件
'''

import unittest
import os
curPath = os.path.abspath(os.path.dirname(__file__))
# 测试用例存放路径
case_path = curPath + '/testcases'

# 获取所有测试用例
def get_all_testcases():
    discover = unittest.defaultTestLoader.discover(case_path, pattern="*.py")
    suite = unittest.TestSuite()
    suite.addTest(discover)
    return suite

if __name__ == '__main__':
    # 运行测试用例
    runner = unittest.TextTestRunner()
    runner.run(get_all_testcases())
